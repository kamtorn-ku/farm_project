#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define E_STEP_PIN         26
#define E_DIR_PIN          28
#define E_ENABLE_PIN       24

#define WATER_PIN 32

#define WATER_PUMP_STAT 0 //WATER_PUMP_STAT=0 active HIGH, WATER_PUMP_STAT=1 active LOW

#if WATER_PUMP_STAT == 1
  int stat_in = 1;
  int stat_out = 0;
 
#else
  int stat_in = 0;
  int stat_out = 1;
#endif

float pool1[3] = {52,30};
float pool2[3] = {52,80};
float pool3[3] = {52,130};
float pool4[3] = {52,180};

float pool5[3] = {104,30};
float pool6[3] = {104,80};
float pool7[3] = {104,130};
float pool8[3] = {104,180};

float pool9[3] = {157,30};
float pool10[3] = {157,80};
float pool11[3] = {157,130};
float pool12[3] = {157,180};

float pool13[3] = {209,30};
float pool14[3] = {209,80};
float pool15[3] = {209,130};
float pool16[3] = {209,180};

float pool17[3] = {260,30};
float pool18[3] = {260,80};
float pool19[3] = {260,130};
float pool20[3] = {260,180};

bool started = false;

float posX = 0;
float posY = 0;
float posZ = 0;

int timingwatering= 5000;

int Xmax,Xmin,Ymax,Ymin,Zmax,Zmin;
//bool startedBlynk = false;

const int NUMBER_OF_FIELDS = 4; // how many comma separated fields we expect
int fieldIndex = 0;            // the current field being received
int values[NUMBER_OF_FIELDS];   // array holding values for all the fields


void setup() {

  pinMode(WATER_PIN , OUTPUT);
  
  pinMode(X_STEP_PIN  , OUTPUT);
  pinMode(X_DIR_PIN    , OUTPUT);
  pinMode(X_ENABLE_PIN    , OUTPUT);

  pinMode(Y_STEP_PIN  , OUTPUT);
  pinMode(Y_DIR_PIN    , OUTPUT);
  pinMode(Y_ENABLE_PIN    , OUTPUT);

  pinMode(Z_STEP_PIN  , OUTPUT);
  pinMode(Z_DIR_PIN    , OUTPUT);
  pinMode(Z_ENABLE_PIN    , OUTPUT);

  pinMode(E_STEP_PIN  , OUTPUT);
  pinMode(E_DIR_PIN    , OUTPUT);
  pinMode(E_ENABLE_PIN    , OUTPUT);


  pinMode(X_MIN_PIN,INPUT);
  pinMode(X_MAX_PIN,INPUT);

  pinMode(Y_MIN_PIN,INPUT);
  pinMode(Y_MAX_PIN,INPUT);

  pinMode(Z_MIN_PIN,INPUT);
  pinMode(Z_MAX_PIN,INPUT);

  Serial.begin(9600);


  Serial.println("STARTING");



  
}





  //////////////////////////   X  Move ///////////////////////////////////////////////////////////

void moveXtomax()
{

   Xmax = digitalRead(X_MAX_PIN);
  if(Xmax == 0){
    Serial.println("X_MAX_PIN");
      //idlemotor();
      posX=262;
    }
    else{

  
  int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(X_DIR_PIN, LOW);
         digitalWrite(E_DIR_PIN, HIGH);
       } else {
         digitalWrite(X_DIR_PIN, HIGH);
         digitalWrite(E_DIR_PIN, LOW);
       }
       delay(1); 
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 16; i++) {
         int t = abs(3200-i) / 200;
          digitalWrite(X_STEP_PIN, HIGH);
         digitalWrite(E_STEP_PIN, HIGH);
         delayMicroseconds(700 + t);
         digitalWrite(X_STEP_PIN, LOW);
         digitalWrite(E_STEP_PIN, LOW);
         delayMicroseconds(700 + t);

       }
    
       
    }
  posX=posX+1;

    }

  

}

void moveXtomin()
{
   Xmin = digitalRead(X_MIN_PIN);
  if(Xmin == 0){
    Serial.println("X_MIN_PIN");
      //idlemotor();
      posX=0;
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(X_DIR_PIN, LOW);
         digitalWrite(E_DIR_PIN, HIGH);
       } else {
         digitalWrite(X_DIR_PIN, HIGH);
         digitalWrite(E_DIR_PIN, LOW);
       }
       delay(1); 
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 16; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(X_STEP_PIN, HIGH);
         digitalWrite(E_STEP_PIN, HIGH);
         delayMicroseconds(700 + t);
         digitalWrite(X_STEP_PIN, LOW);
         digitalWrite(E_STEP_PIN, LOW);
         delayMicroseconds(700 + t);
       
       }
     
       
    }
   posX=posX-1; 

    }

   

}



////////////////////////////////////////////////////////////////

///////////////////////////   Y  Move ///////////////////////////////////////////////////////////

void moveYtomax()
{
   Ymax = digitalRead(Y_MAX_PIN);
  if(Ymax == 0){
    Serial.println("Y_MAX_PIN");
      //idlemotor();
      posY = 200;
      
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Y_DIR_PIN, LOW);
         
       } else {
         digitalWrite(Y_DIR_PIN, HIGH);
         
       }
       delay(1); 
       //dir = 1-dir; 
       for (int i=0; i < 16; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Y_STEP_PIN, HIGH);
         
         delayMicroseconds(700 + t);
         digitalWrite(Y_STEP_PIN, LOW);
         
         delayMicroseconds(700 + t);
        
       }
     
    }
  posY=posY+1;

    }

}

void moveYtomin()
{

  Ymin = digitalRead(Y_MIN_PIN);
  if(Ymin == 0){
      Serial.println("Y_MIN_PIN");
    posY = 0;
          
    }else{

int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Y_DIR_PIN, LOW);
         
       } else {
         digitalWrite(Y_DIR_PIN, HIGH);
         
       }
       delay(1); 
       //dir = 1-dir; 
       for (int i=0; i < 16; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Y_STEP_PIN, HIGH);
         
         delayMicroseconds(700 + t);
         digitalWrite(Y_STEP_PIN, LOW);
         
         delayMicroseconds(700 + t);
       
       }
       
      
    } 
   posY=posY-1; 

    }

}


////////////////////////////////////////////////////////////////

///////////////////////////   Z  Move //////////////////////////

void moveZtomax()
{
  Zmax = digitalRead(Z_MAX_PIN);
  if(Zmax == 0){
    Serial.println("Z_MAX_PIN");
      //idlemotor();
       posZ=28;
    }else{
int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Z_DIR_PIN, LOW);
       } else {
         digitalWrite(Z_DIR_PIN, HIGH);
       }
       delay(1); 
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 16; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Z_STEP_PIN, HIGH);
         delayMicroseconds(700 + t);
         digitalWrite(Z_STEP_PIN, LOW);
         delayMicroseconds(700 + t);
         
       }
       
     
    } 
  posZ=posZ+0.1;

    }

}

void moveZtomin()
{

  Zmin = digitalRead(Z_MIN_PIN);
  if(Zmin == 0){
    Serial.println("Z_MIN_PIN");
      //idlemotor();
      posZ=0;
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Z_DIR_PIN, LOW);
       } else {
         digitalWrite(Z_DIR_PIN, HIGH);
       }
       delay(1); 
       //dir = 1-dir;
       for (int i=0; i < 16; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Z_STEP_PIN, HIGH);
         delayMicroseconds(700 + t);
         digitalWrite(Z_STEP_PIN, LOW);
         delayMicroseconds(700 + t);
         
       }

      
    }
   posZ=posZ-0.1; 
    }

}


////////////////////////////////////////////////////////////////

void moveToPos(float toposX,float toposY,float toposZ)
{
 
  float nowposX = posX;
  float stepposX;
  float nowposY = posY;
  float stepposY;
  float nowposZ = posZ;
  float stepposZ;

  if(toposX > nowposX)
  {
    while(posX<toposX){
      moveXtomax();    
    }   
    Serial.write("OK\n");   
  }
  else if(toposX < nowposX)
        {
         while(posX>toposX){
            moveXtomin(); 
            
         }
         Serial.write("OK\n");
          
        }

   
   


  if(toposY > nowposY)
  {
    
    while(posY<toposY){
      moveYtomax();
      
  
 }
    }  

  
  else if(toposY < nowposY)
        {
         
         while(posY>toposY){
            moveYtomin();  
      
  
  
 } 
 }
         



  if(toposZ > nowposZ)
  {
    
    while(posZ<toposZ){
      moveZtomax();
      
    }  
    
  }
  else if(toposZ < nowposZ)
        {
         
         while(posZ>toposZ){
            moveZtomin();            
           
         }
           
        }
 

}






void loop(){


  if(started == false){
    //startsettingzero();
    takemehome();
    Serial.println(" Ready");
    started  = true;
    }

if( Serial.available())
  {
    char ch = Serial.read();
    if(ch >= '0' && ch <= '9') // is this an ascii digit between 0 and 9?
    {
      // yes, accumulate the value
      values[fieldIndex] = (values[fieldIndex] * 10) + (ch - '0'); 
    }
    else if (ch == ',')  // comma is our separator, so move on to the next field
    {
      if(fieldIndex < NUMBER_OF_FIELDS-1)
        fieldIndex++;   // increment field index
    }
    else
    {
      // any character not a digit or comma ends the acquisition of fields
      // in this example it's the newline character sent by the Serial Monitor
      Serial.print( fieldIndex +1);
      Serial.println(" fields received:");
      for(int i=0; i <= fieldIndex; i++)
      {
        Serial.println(values[i]);
        values[i] = 0; // set the values to zero, ready for the next message
      }
      fieldIndex = 0;  // ready to start over
    }

    if(values[0] == 12){
      
      positiontrack(values[1],values[2],values[3]);   //move to position
      
      }
      else if(values[0] == 10){
        
        startsettingzero();    //start set 0
        }
        else if(values[0] == 11){
          
          takemehome();         //home position
          }else if(values[0] == 14){
            
                  standby();
                          
            }else if(values[0] == 15){
              watering01();
                  
            }else if(values[0] == 16){
              watering02();
                  
            }else if(values[0] == 17){
              watering03();
                  
            }else if(values[0] == 18){
              watering04();
                  
            }else if(values[0] == 19){
              watering05();
                  
            }else if(values[0] == 20){
              watering06();
                  
            }else if(values[0] == 21){
              watering07();
                  
            }else if(values[0] == 22){
              watering08();
                  
            }else if(values[0] == 23){
              watering09();
                  
            }else if(values[0] == 24){
              watering10();
                  
            }else if(values[0] == 25){
              watering11();
                  
            }else if(values[0] == 26){
              watering12();
                  
            }else if(values[0] == 27){
              watering13();
                  
            }else if(values[0] == 28){
              watering14();
                  
            }else if(values[0] == 29){
              watering15();
                  
            }else if(values[0] == 30){
              watering16();
                  
            }else if(values[0] == 31){
              watering17();
                  
            }else if(values[0] == 32){
              watering18();
                  
            }else if(values[0] == 33){
              watering19();
                  
            }else if(values[0] == 34){
              watering20();
                  
            }


    
  }

  
  
  
  }


//
//  void checkmovevalue(float x,float y,float z){
//    
//    moveToPos(values[1],values[2],values[3]);
//    
//    }

  void startsettingzero(){

  
  Xmin = digitalRead(X_MIN_PIN);

  Ymin = digitalRead(Y_MIN_PIN);

  Zmin = digitalRead(Z_MIN_PIN);
  Zmax = digitalRead(Z_MAX_PIN);

while(Zmax!= 0&&Zmin!= 0){
  moveZtomax();
   Zmax = digitalRead(Z_MAX_PIN);
  }


  

  delay(200);

  

  
  while(Xmin!= 0){
    
    moveXtomin();
    Xmin = digitalRead(X_MIN_PIN);
    }

    delay(200);
  while(Ymin!= 0){
    moveYtomin();
    Ymin = digitalRead(Y_MIN_PIN);
    }
    
    delay(200);

  while(Zmin!= 0){
  moveZtomin();
   Zmin = digitalRead(Z_MIN_PIN);
  }

  delay(200);


posX= 0;
posY = 0;
posZ = 0;

  }


  void takemehome(){
     
        moveToPos(posX,posY,5);
        moveToPos(0,0,posZ);
        moveToPos(posX,posY,26);
     // moveToPos(0,42,1);
    
        
        }
  void positiontrack(float x,float y,float z){
    
    moveToPos(x,y,z);
    }

  void standby(){
    
    moveToPos(posX,posY,7);

  } 

  void enablewatering(){
    
    digitalWrite(WATER_PIN,stat_in);

    delay(timingwatering);

    digitalWrite(WATER_PIN,stat_out);
    }

  void watering01(){
    
    moveToPos(pool1[0],pool1[1],posZ);
    enablewatering();
    
    }
     
   void watering02(){
    
    moveToPos(pool2[0],pool2[1],posZ);
    enablewatering();
    
    }

    void watering03(){
    
    moveToPos(pool3[0],pool3[1],posZ);
    enablewatering();
    
    }

    void watering04(){
    
    moveToPos(pool4[0],pool4[1],posZ);
    enablewatering();
    
    }

    void watering05(){
    
    moveToPos(pool5[0],pool5[1],posZ);
    enablewatering();
    
    }

    void watering06(){
    
    moveToPos(pool6[0],pool6[1],posZ);
    enablewatering();
    
    }

    void watering07(){
    
    moveToPos(pool7[0],pool7[1],posZ);
    enablewatering();
    
    }

    void watering08(){
    
    moveToPos(pool8[0],pool8[1],posZ);
    enablewatering();
    
    }
    
    void watering09(){
    
    moveToPos(pool9[0],pool9[1],posZ);
    enablewatering();
    
    }
    
    void watering10(){
    
    moveToPos(pool10[0],pool10[1],posZ);
    enablewatering();
    
    }
    
    void watering11(){
    
    moveToPos(pool11[0],pool11[1],posZ);
    enablewatering();
    
    }
    
    void watering12(){
    
    moveToPos(pool12[0],pool12[1],posZ);
    enablewatering();
    
    }
    
    void watering13(){
    
    moveToPos(pool13[0],pool13[1],posZ);
    enablewatering();
    
    }
    
    void watering14(){
    
    moveToPos(pool14[0],pool14[1],posZ);
    enablewatering();
    
    }
    
    void watering15(){
    
    moveToPos(pool15[0],pool15[1],posZ);
    enablewatering();
    
    }
    
    void watering16(){
    
    moveToPos(pool16[0],pool16[1],posZ);
    enablewatering();
    
    }
    
    void watering17(){
    
    moveToPos(pool17[0],pool17[1],posZ);
    enablewatering();
    
    }
    
    void watering18(){
    
    moveToPos(pool18[0],pool18[1],posZ);
    enablewatering();
    
    }
    
    void watering19(){
    
    moveToPos(pool19[0],pool19[1],posZ);
    enablewatering();
    
    }
    
    void watering20(){
    
    moveToPos(pool20[0],pool20[1],posZ);
    enablewatering();
    
    }
        
    